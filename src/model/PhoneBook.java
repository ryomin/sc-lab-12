package model;

public class PhoneBook {
	
	private String name;
	private String num;
	
	public PhoneBook(String aName, String aNum){
		 
		name = aName;
		num = aNum;
	}
	
	public String toString(){
		return name +",  " + num;
	}
	
	

}
