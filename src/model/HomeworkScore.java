package model;

public class HomeworkScore {
	
	private String name;
	private double score;

	public HomeworkScore(String aName){
		name = aName;
		score = 0;	
	}
	
	public void setScore(double score){
		this.score += score;
	}
	
	public String getName(){
		return name;
	}
	
	public double getScore(){
		return score;
	}
	
	public String toString(){
		return name +": "+score;
	}

}
