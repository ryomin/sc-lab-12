package readfile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;



import model.PhoneBook;

public class PhoneBookReader {

	public static void main(String[] args) {
		
		String filename = "phonebook.txt";
		FileReader fileReader= null;
	 	ArrayList<PhoneBook> phonebooks = new ArrayList<PhoneBook>();
	 	try {
	 		fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			System.out.println("<<<< Phone Book >>>>\n");
			System.out.println("Name ,  Number");
			 for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				 String[] data = line.split(",");
				 String aName = data[0].trim();
				 String aNum = data[1].trim();
			    
				 PhoneBook pb = new PhoneBook(aName, aNum);
				 phonebooks.add(pb);
			 }
			 
			 for(PhoneBook pblist: phonebooks){
					System.out.println(pblist.toString());
				}
				
	 	 }
	 	 catch (FileNotFoundException e){
			 System.err.println("Cannot read file "+filename);
	 	 }	 	
	 	 catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
	 	finally{
			System.out.println("\n\n*********");
			try{
				if(fileReader!= null)
					fileReader.close();
					System.out.println("Close File");
			} catch(IOException e) {
					System.err.println("Error closing files");
			}
		}
		// TODO Auto-generated method stub

	}

}
