package readfile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


import model.HomeworkScore;

public class ExamMain {
	
public static void main(String[] args) {
		
		String filename = "homework.txt";
		FileReader fileReader= null;
	 	ArrayList<HomeworkScore> homeworks = new ArrayList<HomeworkScore>();
	 	ArrayList<HomeworkScore> exams = new ArrayList<HomeworkScore>();
	 	try {
	 		fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			double average;
			System.out.println("<<<< HomeworkScore >>>>\n");
			System.out.println("Name:  Average");
		   
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				String name = data[0].trim();
				Double s1 = Double.parseDouble(data[1].trim());
				Double s2 = Double.parseDouble(data[2].trim());
				Double s3 = Double.parseDouble(data[3].trim());
				Double s4 = Double.parseDouble(data[4].trim());
				Double s5 = Double.parseDouble(data[5].trim());
				double sum = s1+s2+s3+s4+s5;

				HomeworkScore hm = new HomeworkScore(name);
				hm.setScore(sum);
				homeworks.add(hm);
			}
			

			filename = "exam.txt";
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			double average2;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				Double s1 = Double.parseDouble(data[1].trim());
				Double s2 = Double.parseDouble(data[2].trim());
				double sum = s1+s2;
				
				HomeworkScore hm = new HomeworkScore(name);
				hm.setScore(sum);
				exams.add(hm);
				
			}

			for(HomeworkScore scorelist: homeworks){
				average = scorelist.getScore()/5;
				System.out.println(scorelist.getName()+":  "+average+"");				
			}
			System.out.println("\n-----------------------\n");

			System.out.println("<<<<< ExamScores >>>>>\n");
			System.out.println("Name:  Score");
			
			for(HomeworkScore examlist :exams){
				average2 = examlist.getScore()/2;
				System.out.println(examlist.getName()+": "+average2);				
			}


			FileWriter fileWriter = null;
			fileWriter = new FileWriter("average.txt", true);
			BufferedWriter out = new BufferedWriter(fileWriter);


			for(HomeworkScore examlist :exams){
				out.write(examlist.getName()+": "+examlist.getScore()/2.00);
				out.newLine();
			}

			out.flush();
	 	 }
	 	 catch (FileNotFoundException e){
			 System.err.println("Cannot read file "+filename);
	 	 }	 	
	 	 catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
	 	finally{
			try{
				if(fileReader!= null)
					fileReader.close();
					System.out.println("\n\n****Close File****");
			} catch(IOException e) {
					System.err.println("Error closing files");
			}
		}
		// TODO Auto-generated method stub

	}


}
